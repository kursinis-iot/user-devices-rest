# User authentication service

## Dependencies

* Docker Compose

## Starting up

1. Copy `.env.example` file to `.env`, with command: `cp .env.example .env`.
2. Change enviroment variables in `.env` file to your liking.
3. Start docker containers with command `docker compose up -d --build`.

## Description

This service allows users to register & authenticate to our platform.
Upon authentication users are provided JWT token, which other services are able to verify by performing request to this service.

## Routes

### `POST` `http://localhost/register` - Registers an user.

#### JSON payload:

```json
{
    "email": "karolis@kudze.lt",
    "first_name": "Karolis",
    "last_name": "Kraujelis",
    "password": "test1234",
    "password_confirm": "test1234"
}
```

#### Response codes:

```
204 - Success
422 - Validation error
```

### `POST` `http://localhost/login` - Logins an user, returns JWT token.

#### JSON payload:

```json
{
    "email": "karolis@kudze.lt",
    "password": "test1234"
}
```

#### Response codes:

```
200 - Success.
401 - Invalid credentials.
422 - Validation error.
```

#### Success response JSON:

```json
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0IiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdCIsImlhdCI6MTY3NjM5ODg0MiwibmJmIjoxNjc2Mzk4ODQyLCJleHAiOjE2NzY0ODUyNDIsInVzZXIiOnsidXVpZCI6Ijk4Nzc5Yjc5LTdhNDUtNDc3MC1iYzkzLTljNTljOGEwOThkNCIsImZpcnN0X25hbWUiOiJLYXJvbGlzIiwibGFzdF9uYW1lIjoiS3JhdWplbGlzIn19.ecxBU0J7XY-OkxIvZgUw6RrB4aUiFuzoOsc0GpFoK0P59UyH-bYIyEokLDeFqljFYYb0nDfui-tItFxhy05oMw"
}
```

### `POST` `http://localhost/refresh` - Refreshes JWT token for user.

#### JSON payload:

```json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0IiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdCIsImlhdCI6MTY3NjM5ODg0MiwibmJmIjoxNjc2Mzk4ODQyLCJleHAiOjE2NzY0ODUyNDIsInVzZXIiOnsidXVpZCI6Ijk4Nzc5Yjc5LTdhNDUtNDc3MC1iYzkzLTljNTljOGEwOThkNCIsImZpcnN0X25hbWUiOiJLYXJvbGlzIiwibGFzdF9uYW1lIjoiS3JhdWplbGlzIn19.ecxBU0J7XY-OkxIvZgUw6RrB4aUiFuzoOsc0GpFoK0P59UyH-bYIyEokLDeFqljFYYb0nDfui-tItFxhy05oMw"
}
```

#### Response codes:

```
200 - Success.
401 - Token belongs to deleted user.
422 - Validation error.
```

#### Success response JSON:

```json
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0IiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdCIsImlhdCI6MTY3NjM5ODg0MiwibmJmIjoxNjc2Mzk4ODQyLCJleHAiOjE2NzY0ODUyNDIsInVzZXIiOnsidXVpZCI6Ijk4Nzc5Yjc5LTdhNDUtNDc3MC1iYzkzLTljNTljOGEwOThkNCIsImZpcnN0X25hbWUiOiJLYXJvbGlzIiwibGFzdF9uYW1lIjoiS3JhdWplbGlzIn19.ecxBU0J7XY-OkxIvZgUw6RrB4aUiFuzoOsc0GpFoK0P59UyH-bYIyEokLDeFqljFYYb0nDfui-tItFxhy05oMw"
}
```

### `PUT` `http://localhost/validate` - Validates an JWT token. 

#### JSON payload:
```json
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0IiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdCIsImlhdCI6MTY3NjM5ODA1NSwibmJmIjoxNjc2Mzk4MDU1LCJleHAiOjE2NzY0ODQ0NTUsInVzZXIiOnsidXVpZCI6Ijk4Nzc5Yjc5LTdhNDUtNDc3MC1iYzkzLTljNTljOGEwOThkNCIsImZpcnN0X25hbWUiOiJLYXJvbGlzIiwibGFzdF9uYW1lIjoiS3JhdWplbGlzIn19.kIhqEsILSOncdYtA8t64OI7VVbNmIzfJMI9AIktXliUjuvjYac3e06QVbGW8OgeJ4Va9JTd7009Hf94-bXYpbQ"
}
```

#### Response codes:
```
204 - Success.
401 - Failure.
422 - Validation error.
```