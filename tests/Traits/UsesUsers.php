<?php

namespace Tests\Traits;

use App\Models\User;

trait UsesUsers
{
    protected static function setUpUsers(): void
    {
        User::factory()->createMany([
            [
                'uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce9',
                'first_name' => 'Karolis',
                'last_name' => 'Kraujelis',
                'language' => 'lt'
            ]
        ]);
    }

    protected static function deleteUsers(): void
    {
        User::query()->delete();
    }
}