<?php

namespace Tests\Traits;

use Carbon\Carbon;
use Firebase\JWT\JWT;

trait UsesJWT
{
    use WorksWithResources;

    public static function setupJwtKeys(): void
    {
        //We also mock up some jwt time, because otherwise valid token would be expired.
        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();

        $_ENV['JWT_PUBLIC_KEY'] = self::getContentsInResources('jwt.pub');
    }

    protected static function getToken(string $path): string
    {
        return self::getContentsInResources('tokens/' . $path);
    }

    protected static function getValidAccessToken(): string
    {
        return self::getToken('access_valid.jwt');
    }

    protected static function getInvalidAccessToken(): string
    {
        return self::getToken('access_invalid.jwt');
    }

    protected static function getValidRefreshToken(): string
    {
        return self::getToken('refresh_valid.jwt');
    }

    protected static function getInvalidRefreshToken(): string
    {
        return self::getToken('refresh_invalid.jwt');
    }

    protected static function getValidRequestAuthorizationHeaders(): array
    {
        return [
            'Authorization' => 'Bearer ' . self::getValidAccessToken()
        ];
    }
}