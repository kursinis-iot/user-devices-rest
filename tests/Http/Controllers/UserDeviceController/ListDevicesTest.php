<?php

namespace Tests\Http\Controllers\UserDeviceController;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\UsesJWT;
use Tests\Traits\UsesUsers;

class ListDevicesTest extends TestCase
{
    use UsesJWT;
    use UsesUsers;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::setupJwtKeys();
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->setUpUsers();
    }

    protected function tearDown(): void
    {
        $this->deleteUsers();

        parent::tearDown();
    }

    public function testUnauthenticated()
    {
        $this->get('/devices')->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testEmpty()
    {
        $this->get('/devices', $this->getValidRequestAuthorizationHeaders())
            ->assertResponseStatus(Response::HTTP_OK);
    }

    public function testWithInvalidAssociation()
    {
        $this->get('/devices?with[0]=test', $this->getValidRequestAuthorizationHeaders())
            ->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testWithAssociation()
    {
        $this->get('/devices?with[0]=sensors', $this->getValidRequestAuthorizationHeaders())
            ->assertResponseStatus(Response::HTTP_OK);
    }
}