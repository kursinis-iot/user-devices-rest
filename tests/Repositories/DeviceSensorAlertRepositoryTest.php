<?php

namespace Tests\Repositories;

use App\Models\Device;
use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use App\Repositories\DeviceSensorAlertRepository;
use Carbon\Carbon;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

class DeviceSensorAlertRepositoryTest extends TestCase
{
    protected function tearDown(): void
    {
        Device::query()->delete();

        parent::tearDown();
    }

    protected function getInstance(): DeviceSensorAlertRepository
    {
        return app(DeviceSensorAlertRepository::class);
    }

    public static function provideFindDowntimeAlertsToTrigger(): array
    {
        return [
            '#1 in empty database should return empty' => [
                //Expected alert uuids
                []
            ],
            '#2 random test' => [
                //Expected alert uuids
                [
                    'online_two_hr_ago_active',
                    'online_two_hr_ago_active2',
                    'online_half_hr_ago_active_15m_wait'
                ],
                //Device data
                [
                    [
                        'uuid' => 'never_online',
                        'last_sensor_updated_at' => null,
                    ],
                    [
                        'uuid' => 'online_now',
                        'last_sensor_updated_at' => Carbon::now(),
                    ],
                    [
                        'uuid' => 'online_half_hr_ago',
                        'last_sensor_updated_at' => Carbon::now()->subMinutes(30),
                    ],
                    [
                        'uuid' => 'online_two_hr_ago',
                        'last_sensor_updated_at' => Carbon::now()->subHours(2),
                    ]
                ],
                //Device sensor data
                [
                    [
                        'uuid' => 'never_online',
                        'device_uuid' => 'never_online',
                    ],
                    [
                        'uuid' => 'online_now',
                        'device_uuid' => 'online_now',
                    ],
                    [
                        'uuid' => 'online_half_hr_ago',
                        'device_uuid' => 'online_half_hr_ago',
                    ],
                    [
                        'uuid' => 'online_two_hr_ago',
                        'device_uuid' => 'online_two_hr_ago',
                    ],
                ],
                //Device sensor alert
                [
                    [
                        'uuid' => 'never_online_active',
                        'device_sensor_uuid' => 'never_online',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_now_active',
                        'device_sensor_uuid' => 'online_now',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_half_hr_ago_active',
                        'device_sensor_uuid' => 'online_half_hr_ago',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_half_hr_ago_active_15m_wait',
                        'device_sensor_uuid' => 'online_half_hr_ago',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '900',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_two_hr_ago_active',
                        'device_sensor_uuid' => 'online_two_hr_ago',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_two_hr_ago_active2',
                        'device_sensor_uuid' => 'online_two_hr_ago',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => Carbon::now()->subMinutes(61),
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_two_hr_ago_active_wrong_type',
                        'device_sensor_uuid' => 'online_two_hr_ago',
                        'type' => DeviceSensorAlert::RANGE_TYPE,
                        'payload' => '{"min": 0, "max": 100}',
                        'last_triggered_at' => null,
                        'interval' => 3600
                    ],
                    [
                        'uuid' => 'online_two_hr_ago_not_active',
                        'device_sensor_uuid' => 'online_two_hr_ago',
                        'type' => DeviceSensorAlert::DOWNTIME_TYPE,
                        'payload' => '3600',
                        'last_triggered_at' => Carbon::now(),
                        'interval' => 3600
                    ],
                ]
            ]
        ];
    }

    #[DataProvider('provideFindDowntimeAlertsToTrigger')]
    public function testFindDowntimeAlertsToTrigger(
        array $expectedAlertUuids,
        array $deviceData = [],
        array $deviceSensorData = [],
        array $deviceSensorAlert = []
    )
    {
        Device::factory()->createMany($deviceData);
        DeviceSensor::factory()->createMany($deviceSensorData);
        DeviceSensorAlert::factory()->createMany($deviceSensorAlert);

        $instance = $this->getInstance();
        $result = $instance->findDowntimeAlertsToTrigger();

        $this->assertSame(count($expectedAlertUuids), count($result));
        /** @var DeviceSensorAlert $alert */
        foreach ($result as $alert)
            $this->assertContains($alert->getKey(), $expectedAlertUuids);
    }
}