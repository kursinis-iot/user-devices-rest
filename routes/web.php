<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/devices', ['as' => 'listDevices', 'uses' => 'UserDeviceController@listDevices']);
    $router->post('/devices', ['as' => 'createDevice', 'uses' => 'UserDeviceController@createDeviceForUser']);
    $router->delete('/devices', ['as' => 'deleteDevice', 'uses' => 'UserDeviceController@deleteUserDevice']);

    $router->put('/devices/{deviceUuid}/configure', ['as' => 'configureDevice', 'uses' => 'UserDeviceController@configureDevice']);

    $router->get('/devices/{deviceUuid}/sensor/{sensorUuid}/log', ['as' => 'browseSensorHistory', 'uses' => 'DeviceSensorController@browseHistory']);
    $router->put('/devices/{deviceUuid}/sensor/{sensorUuid}/alerts', ['as' => 'configureSensorAlerts', 'uses' => 'DeviceSensorController@configureAlerts']);

    $router->get('/anomalies/models', ['as' => 'listAnomalyModels', 'uses' => 'AnomalyModelsController@listModels']);
});