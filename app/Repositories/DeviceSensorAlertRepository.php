<?php

namespace App\Repositories;

use App\Models\DeviceSensorAlert;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class DeviceSensorAlertRepository
{
    protected static function whereUnexpired(Builder $query): void
    {
        $query->whereNull('last_triggered_at')
            ->orWhereRaw('TIMESTAMPADD(SECOND, `interval`, `last_triggered_at`) < NOW()');
    }

    /**
     * Returns downtime alerts that should be triggered.
     *
     * @param array $with - relations to load.
     * @return Collection
     */
    public function findDowntimeAlertsToTrigger(array $with = []): Collection
    {
        return DeviceSensorAlert::with($with)
            ->where('type', DeviceSensorAlert::DOWNTIME_TYPE)
            ->where(fn(Builder $query) => self::whereUnexpired($query))
            ->whereHas('sensor.device', function (Builder $query) {
                $query->whereNotNull('last_sensor_updated_at')
                    ->whereRaw('NOW() > TIMESTAMPADD(SECOND, `payload`, `last_sensor_updated_at`)');
            })->get();
    }

    public function findNotTriggeredAlertsBySensors(array $sensorUuids, array $with = [], array $skipTypes = []): Collection
    {
        $query = DeviceSensorAlert::with($with)
            ->whereIn('device_sensor_uuid', $sensorUuids)
            ->where(fn(Builder $query) => self::whereUnexpired($query));

        if (!empty($skipTypes))
            $query->whereNotIn('type', $skipTypes);

        return $query->get();
    }

    /**
     * Marks alert as triggered.
     *
     * @param string $uuid - uuid of alert
     * @return void
     */
    public function markAlertAsTriggered(string $uuid): void
    {
        DeviceSensorAlert::query()->where('uuid', $uuid)->update([
            'last_triggered_at' => Carbon::now('UTC')->toDateTimeString()
        ]);
    }
}