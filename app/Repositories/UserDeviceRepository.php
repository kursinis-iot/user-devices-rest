<?php

namespace App\Repositories;

use App\Models\UserDevice;
use Carbon\Carbon;

class UserDeviceRepository
{

    public function insert(string $userUuid, string $deviceUuid, string $role): void
    {
        UserDevice::query()->insert([
            'user_id' => $userUuid,
            'device_id' => $deviceUuid,
            'role' => $role,
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

}