<?php

namespace App\Repositories;

use App\Models\DeviceSensorLog;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class DeviceSensorLogRepository
{
    public function bulkInsert(array $payload, string $date): void
    {
        $data = array_map(
            fn(string $sensorUuid, $value) => [
                'uuid' => Str::orderedUuid(),
                'device_sensor_uuid' => $sensorUuid,
                'data' => json_encode($value),
                'created_at' => $date
            ],
            array_keys($payload),
            array_values($payload)
        );

        DeviceSensorLog::query()->insertOrIgnore($data);
    }

    public function markAnomalies(string $deviceUuid, string $date): void
    {
        DeviceSensorLog::query()
            ->whereHas('sensor', fn(Builder $builder) => $builder->where('device_uuid', $deviceUuid))
            ->where('created_at', $date)
            ->update([
                'anomaly' => true
            ]);
    }
}