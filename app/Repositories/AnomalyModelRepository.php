<?php

namespace App\Repositories;

use App\Models\AnomalyModel;
use Illuminate\Database\Eloquent\Collection;

class AnomalyModelRepository
{
    public function fetchAll(): Collection
    {
        return AnomalyModel::all();
    }
}