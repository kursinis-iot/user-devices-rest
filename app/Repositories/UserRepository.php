<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function delete(string $uuid): void
    {
        User::query()->where('uuid', $uuid)->delete();
    }

    public function upsert(string $uuid, string $firstName, string $lastName, string $language): void
    {
        User::query()->upsert([
            'uuid' => $uuid,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'language' => $language,
        ], ['uuid'], ['first_name', 'last_name', 'language']);
    }

    public function findByUuid(string $uuid): ?User
    {
        /** @var ?User */
        return User::query()->find($uuid);
    }
}