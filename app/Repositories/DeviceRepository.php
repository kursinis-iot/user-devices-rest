<?php

namespace App\Repositories;

use App\Models\Device;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DeviceRepository
{
    public function paginateUserDevices(string $userUuid, int $page, int $pageSize, string $orderBy, string $orderByDirection, array $with = []): LengthAwarePaginator
    {
        return Device::with($with)->whereHas('users', function ($query) use ($userUuid) {
            $query->where('uuid', $userUuid);
        })->orderBy($orderBy, $orderByDirection)->paginate($pageSize, page: $page);
    }

    public function markDeviceReceivedData(string $deviceUuid, string $date): void
    {
        Device::query()->where('uuid', $deviceUuid)->update([
            'last_sensor_updated_at' => $date,
            'status' => Device::DEVICE_STATUS_ONLINE
        ]);
    }

    public function toggleOffline(): void
    {
        Device::query()->where('last_sensor_updated_at', '<', DB::raw('NOW() - INTERVAL toggle_offline_after_seconds SECOND'))
            ->where('status', Device::DEVICE_STATUS_ONLINE)
            ->update(['status' => Device::DEVICE_STATUS_OFFLINE]);
    }

    public function create(
        string $userUuid, string $title, int $deviceToggleOfflineSeconds,
        string $config, string $apiKeyHash, ?string $anomalyModelUuid
    ): Device
    {
        $data = [
            'title' => $title,
            'added_by_user' => $userUuid,
            'toggle_offline_after_seconds' => $deviceToggleOfflineSeconds,
            'config' => json_decode($config, true),
            'api_key_hash' => $apiKeyHash,
            'anomaly_model_uuid' => $anomalyModelUuid
        ];

        /** @var Device */
        return Device::query()->create($data);
    }

    public function configureDevice(string $deviceUuid, string $config, ?string $anomalyModelUuid): void
    {
        Device::query()->where('uuid', $deviceUuid)->update(['config' => $config, 'anomaly_model_uuid' => $anomalyModelUuid]);
    }

    public function deleteDevice(string $deviceUuid): void
    {
        Device::query()->where('uuid', $deviceUuid)->delete();
    }
}