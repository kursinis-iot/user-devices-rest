<?php

namespace App\Console\Commands;

use App\Repositories\DeviceRepository;
use App\Repositories\DeviceSensorLogRepository;
use App\Service\Alert\Handler\AlertHandlerService;
use Carbon\Carbon;
use Kudze\KafkaConsumerProducer\Services\KafkaConsumer;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;
use Throwable;

class ConsumeDeviceSensorLogCommand extends AbstractConsumerJsonCommand
{
    protected $signature = "consume:devices:sensor:log";
    protected $description = "Consumes device sensor log.";

    public function __construct(
        KafkaConsumer                       $consumer,
        protected DeviceRepository          $deviceRepository,
        protected DeviceSensorLogRepository $deviceSensorLogRepository,
        protected AlertHandlerService       $alertHandlerService
    )
    {
        parent::__construct($consumer);
    }

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_DEVICE_SENSOR_LOG_TOPIC')];
    }

    protected function processJsonMessage(Message $message, ?array $payload): void
    {
        ['deviceKey' => $deviceUuid, 'date' => $date] = json_decode($message->key, true);
        $date = Carbon::parse($date)->toDateTimeString();

        //First we insert into MySQL.
        $this->deviceRepository->markDeviceReceivedData($deviceUuid, $date);
        $this->deviceSensorLogRepository->bulkInsert($payload, $date);

        //Then we handle the alerts
        //TODO: if we add more complex alerts it'd make sense to move to queue worker, but for now is perfectly fine.
        try {
            $this->alertHandlerService->handle($payload, $message->timestamp);
        } catch (Throwable $throwable) {
            $this->io->warning($throwable->getMessage());
        }
    }
}