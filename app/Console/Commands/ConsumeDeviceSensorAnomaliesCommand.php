<?php

namespace App\Console\Commands;

use App\Repositories\DeviceSensorLogRepository;
use Carbon\Carbon;
use Kudze\KafkaConsumerProducer\Services\KafkaConsumer;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;

class ConsumeDeviceSensorAnomaliesCommand extends AbstractConsumerJsonCommand
{
    protected $signature = "consume:devices:sensor:anomalies";
    protected $description = "Consumes device sensor anomalies";

    public function __construct(
        KafkaConsumer                       $consumer,
        protected DeviceSensorLogRepository $deviceSensorLogRepository
    )
    {
        parent::__construct($consumer);
    }

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_DEVICE_SENSOR_ANOMALIES_TOPIC')];
    }

    protected function processJsonMessage(Message $message, ?array $payload): void
    {
        $this->deviceSensorLogRepository->markAnomalies(
            $payload['deviceUuid'],
            Carbon::parse($payload['date'])->toDateTimeString()
        );
    }
}