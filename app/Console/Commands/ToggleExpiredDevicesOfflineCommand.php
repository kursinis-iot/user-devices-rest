<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Repositories\DeviceRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ToggleExpiredDevicesOfflineCommand extends AbstractCommand
{
    protected $signature = "index:devices:offline";
    protected $description = "Indexes the offline devices";

    public function __construct(
        protected DeviceRepository $deviceRepository
    )
    {
        parent::__construct();
    }

    public function handle(): int
    {
        parent::handle();

        $this->deviceRepository->toggleOffline();
        $this->io->success("Done");

        return self::SUCCESS;
    }
}