<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Repositories\UserRepository;
use Kudze\KafkaConsumerProducer\Services\KafkaConsumer;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;

class ConsumeRegisteredUsersCommand extends AbstractConsumerJsonCommand
{
    protected $signature = "consume:users";
    protected $description = "Consumes new registered users from kafka!";

    public function __construct(
        KafkaConsumer            $consumer,
        protected UserRepository $userRepository
    )
    {
        parent::__construct($consumer);
    }

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_USER_REGISTERED_TOPIC')];
    }

    protected function processJsonMessage(Message $message, ?array $payload): void
    {
        if ($payload === null) {
            $this->io->writeln("Offset: $message->offset, user $message->key is to be deleted...");
            $this->userRepository->delete($message->key);
            return;
        }

        $this->io->writeln("Offset: $message->offset, user $message->key is to be upserted...");
        $this->userRepository->upsert(
            $payload['uuid'],
            $payload['first_name'],
            $payload['last_name'],
            $payload['language'] ?? 'lt',
        );
    }
}