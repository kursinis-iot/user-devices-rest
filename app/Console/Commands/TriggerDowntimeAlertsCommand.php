<?php

namespace App\Console\Commands;

use App\Jobs\Alert\TriggerDowntimeAlertsJob;
use App\Repositories\DeviceSensorAlertRepository;
use App\Service\Alert\AlertTriggererService;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TriggerDowntimeAlertsCommand extends AbstractCommand
{
    protected $signature = "trigger:downtime:alerts";
    protected $description = "Triggers downtime alerts";

    public function __construct(
        protected DeviceSensorAlertRepository $alertRepository,
        protected AlertTriggererService       $alertTriggerer,
    )
    {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->info("Dispatching job...");
        dispatch(new TriggerDowntimeAlertsJob());
        return self::SUCCESS;
    }
}