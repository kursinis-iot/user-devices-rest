<?php

namespace App\Console;

use App\Console\Commands\ConsumeDeviceSensorAnomaliesCommand;
use App\Console\Commands\ConsumeDeviceSensorLogCommand;
use App\Console\Commands\ConsumeRegisteredUsersCommand;
use App\Console\Commands\ToggleExpiredDevicesOfflineCommand;
use App\Console\Commands\TriggerDowntimeAlertsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ConsumeRegisteredUsersCommand::class,
        ConsumeDeviceSensorLogCommand::class,
        ConsumeDeviceSensorAnomaliesCommand::class,
        ToggleExpiredDevicesOfflineCommand::class,
        TriggerDowntimeAlertsCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(ToggleExpiredDevicesOfflineCommand::class)->everyMinute()->withoutOverlapping()->sendOutputTo('storage/logs/cron_toggle_offline.log', true);
        $schedule->command(TriggerDowntimeAlertsCommand::class)->everyMinute()->withoutOverlapping()->sendOutputTo('storage/logs/cron_downtime_alerts.log', true);
    }
}