<?php

namespace App\Providers;

use App\Models\Device;
use Flow\JSONPath\JSONPathException;
use Flow\JSONPath\JSONPathLexer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Kudze\KafkaConsumerProducer\Services\KafkaProducer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(KafkaProducer::class, function () {
            $producer = new KafkaProducer();
            $producer->init();
            return $producer;
        });

        Validator::extend('jsonpath', function ($attribute, $value, $parameters) {
            if (!is_string($value))
                return false;

            try {
                new JSONPathLexer($value);
                return true;
            } catch (JSONPathException) {
                return false;
            }
        }, "Is not a valid JSONPath query!");

        Validator::extend('device_role', function ($attribute, $value, $parameters) {
            [$role] = $parameters;

            return Device::query()->where('uuid', $value)->whereHas('users', function(Builder $builder) use ($role) {
                $builder->where('role', $role)->where('uuid', Auth::user()->getAuthIdentifier());
            })->exists();
        }, "You don't have required role for this device!");
    }
}
