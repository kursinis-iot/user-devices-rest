<?php

namespace App\Providers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Kudze\AccessTokenValidator\Service\AccessTokenValidator;
use Kudze\LumenAccessTokenValidator\Traits\DecodesTokens;
use Throwable;

class AuthServiceProvider extends ServiceProvider
{
    use DecodesTokens;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function (Request $request) {
            /** @var AccessTokenValidator $validator */
            $validator = app(AccessTokenValidator::class);
            /** @var UserRepository $userRepository */
            $userRepository = app(UserRepository::class);

            try {
                $user = $this->decodeUserFromToken($validator, $request);
                return $userRepository->findByUuid($user->uuid);
            } catch (Throwable) {
                return null;
            }
        });
    }
}
