<?php

namespace App\Http\Controllers;

use App\Repositories\AnomalyModelRepository;
use Illuminate\Http\JsonResponse;
use Kudze\LumenBaseController\Http\Controllers\Controller;

class AnomalyModelsController extends Controller
{
    public function __construct(
        protected AnomalyModelRepository $repository
    )
    {

    }

    public function listModels(): JsonResponse
    {
        $models = $this->repository->fetchAll();

        return response()->json($models);
    }
}