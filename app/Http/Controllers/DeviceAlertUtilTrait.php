<?php

namespace App\Http\Controllers;

use App\Models\DeviceSensorAlert;
use Illuminate\Validation\Rules\In;

const ALERT_VALIDATION_RULES = [
    '' => 'nullable|array',
    '.*.interval' => 'required|integer|min:1',
    '.*.title' => 'required|string|min:3|max:255',
    '.*.type' => ['required', 'string', new In(DeviceSensorAlert::TYPES)],
    '.*.payload' => 'required|string|json', //TODO: validation for payload along type.
    '.*.email_recipients' => 'required|array|min:1',
    '.*.email_recipients.*' => 'required|array:email,first_name,last_name,language',
    '.*.email_recipients.*.email' => 'required|email|max:255',
    '.*.email_recipients.*.first_name' => 'required|string|min:3|max:255',
    '.*.email_recipients.*.last_name' => 'required|string|min:3|max:255',
    '.*.email_recipients.*.language' => 'required|string|in:lt,en',
];

trait DeviceAlertUtilTrait
{
    public function getAlertValidationRules(string $prefix = 'alerts'): array
    {
        $rules = [];

        foreach (ALERT_VALIDATION_RULES as $key => $value)
            $rules[$prefix . $key] = $value;

        return $rules;
    }
}