<?php

namespace App\Http\Controllers;

use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use App\Models\User;
use App\Repositories\DeviceRepository;
use App\Service\DeviceSensorLogHistoryService;
use App\Service\DeviceService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Kudze\LumenBaseController\Http\Controllers\Controller;

class DeviceSensorController extends Controller
{
    use DeviceAlertUtilTrait;

    /**
     * @throws ValidationException
     */
    public function browseHistory(
        string                        $deviceUuid,
        string                        $sensorUuid,
        Request                       $request,
        DeviceSensorLogHistoryService $historyService
    ): JsonResponse
    {
        $request->query->set("device_uuid", $deviceUuid);
        $request->query->set("sensor_uuid", $sensorUuid);

        $data = $this->validate(
            $request,
            [
                'device_uuid' => 'required|string|uuid|exists:device,uuid',
                'sensor_uuid' => 'required|string|uuid|exists:device_sensor,uuid',
                'from' => 'required|date|date_format:Y-m-d H:i:s',
                'till' => 'required|date|date_format:Y-m-d H:i:s|after_or_equal:from',
                'timezone' => 'required|timezone',
                'granularity' => 'required|in:raw,minute,hour,day'
            ]
        );

        $carbonFrom = Carbon::parse($data['from'], $data['timezone'])->setTimezone('UTC');
        $carbonTill = Carbon::parse($data['till'], $data['timezone'])->setTimezone('UTC');

        $this->validateTimeframe($request, $carbonFrom, $carbonTill, $data['granularity']);

        $data['from'] = $carbonFrom->toDateTimeString();
        $data['till'] = $carbonTill->toDateTimeString();

        /** @var User $user */
        return new JsonResponse($this->fetchDeviceLog($data, $historyService));
    }

    /**
     * @throws ValidationException
     */
    public function configureAlerts(string $deviceUuid, string $sensorUuid, Request $request, DeviceService $deviceService): Response
    {
        $request->query->set("device_uuid", $deviceUuid);
        $request->query->set("sensor_uuid", $sensorUuid);

        $data = $this->validate(
            $request,
            [
                'device_uuid' => 'required|string|uuid',
                'sensor_uuid' => 'required|string|uuid',
                ...$this->getAlertValidationRules()
            ]
        );

        /** @var User $user */
        $user = Auth::user();
        /** @var DeviceSensor $sensor */
        $sensor = DeviceSensor::query()->whereHas('device.users', function (Builder $query) use ($user) {
            $query->where('uuid', $user->getKey());
        })->where('device_uuid', $data['device_uuid'])->find($data['sensor_uuid']);

        if ($sensor === null)
            throw ValidationException::withMessages([
                'device_uuid' => "Sensor not found",
                'sensor_uuid' => "Sensor not found",
            ]);

        //We delete all alerts for device:
        DeviceSensorAlert::query()->where('device_sensor_uuid', $sensor->getKey())->delete();

        $alerts = null;
        if (array_key_exists('alerts', $data))
            $alerts = $data['alerts'];

        $deviceService->configureDeviceSensorAlerts($alerts, $sensor);

        return $this->noContentResponse();
    }

    /**
     * @throws ValidationException
     */
    protected function validateTimeframe(Request $request, Carbon $from, Carbon $till, string $granularity): void
    {
        switch ($granularity) {
            case "raw":
                if (!$from->between($till->copy()->subHour(), $till))
                    $this->throwValidationException(
                        $request,
                        ValidationException::withMessages([
                            'timeframe' => "Must be less than or equal to 1 hour",
                        ])->validator
                    );
                break;
            case "minute":
                if (!$from->between($till->copy()->subDays(), $till))
                    $this->throwValidationException(
                        $request,
                        ValidationException::withMessages([
                            'timeframe' => "Must be less than or equal to 1 day",
                        ])->validator
                    );
                break;
            case "hour":
                if (!$from->between($till->copy()->subDays(60), $till))
                    $this->throwValidationException(
                        $request,
                        ValidationException::withMessages([
                            'timeframe' => "Must be less than or equal to 60 days",
                        ])->validator
                    );
                break;
            case "day":
                if (!$from->between($till->copy()->subDays(366), $till))
                    $this->throwValidationException(
                        $request,
                        ValidationException::withMessages([
                            'timeframe' => "Must be less than or equal to 366 days",
                        ])->validator
                    );
                break;
        }
    }

    protected function fetchDeviceLog(array $data, DeviceSensorLogHistoryService $historyService): Collection
    {
        /** @var User $user */
        $user = Auth::user();
        [
            'sensor_uuid' => $sensorUuid,
            'from' => $from,
            'till' => $till,
            'granularity' => $granularity
        ] = $data;

        return $historyService->computeDeviceSensorLog($sensorUuid, $from, $till, $granularity, $user);
    }
}