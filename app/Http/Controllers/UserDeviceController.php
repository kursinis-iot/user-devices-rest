<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\User;
use App\Service\DeviceService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;
use Kudze\LumenPaginatedController\Http\Controllers\PaginatedUuidController;

class UserDeviceController extends PaginatedUuidController
{
    use DeviceAlertUtilTrait;

    public function __construct(
        protected DeviceService $deviceService
    )
    {

    }

    protected function getValidOrderByKeys(): array
    {
        return Device::VALID_ORDER_BY;
    }

    /**
     * @throws ValidationException
     */
    public function listDevices(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'pageSize' => $pageSize,
            'orderBy' => $orderBy,
            'orderByDirection' => $orderByDirection,
            'with' => $with
        ] = $this->validatePaginatedRequest($request, [
            'with' => 'array',
            'with.*' => ['required', 'distinct', 'string', new In([
                'sensors',
                'sensors.minuteOfLogs',
                'sensors.alerts',
                'sensors.alerts.emailRecipients',
                'anomalyModel'
            ])]
        ]) + [
            'with' => []
        ];

        return new JsonResponse(
            $this->deviceService->paginateUserDevices(Auth::id(), $page, $pageSize, $orderBy, $orderByDirection, $with)
        );
    }

    /**
     * @throws ValidationException
     */
    public function createDeviceForUser(Request $request): JsonResponse|Response
    {
        //TODO: sensor config.
        $data = $this->validate($request, [
            'title' => "required|string|min:3|max:255",
            'key' => "nullable|string|min:36|max:255",
            'offline_time' => 'required|integer|min:1',
            'sensors' => "required|array|min:1",
            'sensors.*.title' => 'required|string|min:3|max:255',
            'sensors.*.query' => 'required|string|max:255|jsonpath',
            ...$this->getAlertValidationRules('sensors.*.alerts'),
            'config' => 'required|string|json',
            'anomaly_model_uuid' => 'nullable|string|exists:anomaly_models,uuid',
        ]);

        $apiKey = array_key_exists('key', $data) ? $data['key'] : Str::uuid();

        /** @var User $user */
        $user = Auth::user();
        $device = $this->deviceService->create(
            $user,
            $data['title'],
            $data['offline_time'],
            $data['sensors'],
            $data['config'],
            $apiKey,
            array_key_exists('anomaly_model_uuid', $data) ? $data['anomaly_model_uuid'] : null
        );

        return new JsonResponse([
            'device' => $device->toArray(),
            'api_key' => $apiKey
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function deleteUserDevice(Request $request): Response
    {
        ['uuid' => $uuid] = $this->validate($request, [
            'uuid' => 'uuid|device_role:owner'
        ]);

        $this->deviceService->deleteDevice($uuid);

        return $this->noContentResponse();
    }

    /**
     * @throws ValidationException
     */
    public function configureDevice(string $deviceUuid, Request $request): Response
    {
        $request->query->set('device_uuid', $deviceUuid);

        $data = $this->validate($request, [
            'device_uuid' => 'required|uuid|device_role:owner',
            'config' => 'required|string|json',
            'anomaly_model_uuid' => 'nullable|string|exists:anomaly_models,uuid',
        ]);

        $this->deviceService->configureDevice(
            $deviceUuid,
            $data['config'],
            array_key_exists('anomaly_model_uuid', $data) ? $data['anomaly_model_uuid'] : null
        );

        return $this->noContentResponse();
    }
}