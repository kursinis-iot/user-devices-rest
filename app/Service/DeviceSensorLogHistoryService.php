<?php

namespace App\Service;

use App\Models\DeviceSensorLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class DeviceSensorLogHistoryService
{
    public function computeDeviceSensorLog(string $sensorUuid, string $from, string $till, string $granularity, User $user): Collection
    {
        if ($granularity === 'raw')
            return $this->fetchDeviceSensorLog($sensorUuid, $from, $till, $user);

        return $this->aggregateDeviceLog($sensorUuid, $from, $till, $granularity, $user);
    }

    protected function fetchDeviceSensorLog(string $sensorUuid, string $from, string $till, User $user): Collection
    {
        return DeviceSensorLog::query()->where('device_sensor_uuid', $sensorUuid)->whereHas('sensor.device.users', function (Builder $query) use ($user) {
            $query->where('uuid', $user->getKey());
        })->where('created_at', ">=", $from)->where('created_at', '<=', $till)->get();
    }

    protected function aggregateDeviceLog(string $sensorUuid, string $from, string $till, string $granularity, User $user): Collection
    {
        $carbonFrom = Carbon::parse($from);
        $carbonTill = Carbon::parse($till);
        $current = $carbonFrom->copy();

        $buckets = new Collection();

        while ($current->lessThanOrEqualTo($carbonTill)) {
            $nextCurrent = $current->copy()->add($granularity, 1);

            $data = $this->fetchDeviceSensorLog(
                $sensorUuid,
                $current->toDateTimeString(),
                ($nextCurrent->greaterThan($carbonTill) ? $carbonTill : $nextCurrent)->toDateTimeString(),
                $user
            );

            $buckets->add([
                'uuid' => null,
                'device_sensor_uuid' => $sensorUuid,
                'created_at' => $current->toDateTimeString(),
                'data' => $this->computeAggregatedValue(
                    $data->map(fn(DeviceSensorLog $entry) => $entry->getAttribute('data'))
                ),
            ]);

            $current = $nextCurrent;
        }

        return $buckets;
    }

    protected function computeAggregatedValue(Collection $data): array|float
    {
        if ($data->isEmpty())
            return 0;

        $first = $data->get(0);
        if (is_null($first))
            return 0;

        if (is_numeric($first))
            return $this->computeScalarAggregatedValue($data);

        if (is_array($first))
            return $this->computeArrayAggregatedValue($data);

        return 0;
    }

    protected function computeScalarAggregatedValue(Collection $data): float
    {
        $avg = 0.;
        foreach ($data as $num)
            $avg += $num;

        return $avg / $data->count();
    }

    protected function computeArrayAggregatedValue(Collection $data): array
    {
        $avg = array_map(fn() => 0., $data->get(0));

        foreach ($data as $entry) {
            $idx = 0;
            foreach ($entry as $num) {
                $avg[$idx] += $num;
                $idx++;
            }
        }

        return array_map(fn($num) => $num / $data->count(), $avg);
    }
}