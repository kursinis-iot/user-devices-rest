<?php

namespace App\Service;

use App\Models\Device;
use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use App\Models\User;
use App\Models\UserDevice;
use App\Repositories\DeviceRepository;
use App\Repositories\UserDeviceRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class DeviceService
{
    public function __construct(
        protected DeviceRepository     $deviceRepository,
        protected UserDeviceRepository $userDeviceRepository,
        protected ApiKeyHasher         $apiKeyHasher,
    )
    {
    }

    public function create(
        User    $user,
        string  $title,
        int     $toggleOfflineSeconds,
        array   $sensors,
        string  $config,
        string  $apiKey,
        ?string $anomalyModelUuid
    ): Device
    {
        $userUuid = $user->getKey();

        $device = $this->deviceRepository->create(
            $userUuid,
            $title,
            $toggleOfflineSeconds,
            $config,
            $this->apiKeyHasher->hash($apiKey),
            $anomalyModelUuid
        );

        $this->userDeviceRepository->insert(
            $userUuid,
            $device->getKey(),
            UserDevice::ROLE_OWNER
        );

        //TODO: if we have time lets split into proper crud api.
        foreach ($sensors as $sensorData) {
            /** @var DeviceSensor $sensorModel */
            $sensorModel = $device->sensors()->create([
                'title' => $sensorData['title'],
                'jsonpath_query' => $sensorData['query']
            ]);

            $alerts = $sensorData['alerts'];
            if ($alerts !== null)
                $this->configureDeviceSensorAlerts($alerts, $sensorModel);
        }

        return $device;
    }

    public function paginateUserDevices(string $userUuid, int $page, int $pageSize, string $orderBy, string $orderByDirection, array $with = []): LengthAwarePaginator
    {
        return $this->deviceRepository->paginateUserDevices($userUuid, $page, $pageSize, $orderBy, $orderByDirection, $with);
    }

    public function configureDevice(string $deviceUuid, string $config, ?string $anomalyModelUuid): void
    {
        $this->deviceRepository->configureDevice($deviceUuid, $config, $anomalyModelUuid);
    }

    public function deleteDevice(string $deviceUuid): void
    {
        $this->deviceRepository->deleteDevice($deviceUuid);
    }

    //TODO: this also should be split to crud.
    public function configureDeviceSensorAlerts(array $alerts, DeviceSensor $sensorModel): void
    {
        foreach ($alerts as $alertData) {
            /** @var DeviceSensorAlert $alertModel */
            $alertModel = $sensorModel->alerts()->create([
                'title' => $alertData['title'],
                'interval' => $alertData['interval'],
                'type' => $alertData['type'],
                'payload' => json_decode($alertData['payload'], true)
            ]);

            $alertModel->emailRecipients()->createMany($alertData['email_recipients']);
        }
    }
}