<?php

namespace App\Service\Alert;

use App\Jobs\Kafka\EmitAlertTriggeredEmailJob;
use App\Models\DeviceSensorAlert;
use App\Repositories\DeviceSensorAlertRepository;

class AlertTriggererService
{
    public function __construct(
        protected DeviceSensorAlertRepository $repository
    )
    {

    }

    public function failAlert(DeviceSensorAlert $alert, ?array $sensorValue = null, ?int $timestamp = null, ?string $message = null): void
    {
        $this->repository->markAlertAsTriggered($alert->getKey());

        dispatch(new EmitAlertTriggeredEmailJob($alert, $sensorValue, $timestamp, $message));
    }
}