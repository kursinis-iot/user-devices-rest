<?php

namespace App\Service\Alert\Handler;

use App\Models\DeviceSensorAlert;
use App\Service\Alert\AlertTriggererService;

abstract class AbstractAlertHandler
{
    public function __construct(
        protected AlertTriggererService $alertTriggerer
    )
    {

    }

    public abstract function handle(DeviceSensorAlert $alert, mixed $sensorValue, int $timestamp);

    /**
     * @param mixed $sensorValue
     * @param callable $callback - function ($value): bool, if returns false traverse will be stopped. $value is a scalar or null.
     * @return bool - internal
     */
    protected function traverseSensorValue(mixed $sensorValue, callable $callback): bool
    {
        if (is_scalar($sensorValue) || is_null($sensorValue))
            return $callback($sensorValue);

        foreach ($sensorValue as $value)
            if (!$this->traverseSensorValue($value, $callback))
                return false;

        return true;
    }
}