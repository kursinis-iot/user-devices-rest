<?php

namespace App\Service\Alert\Handler;

use App\Models\DeviceSensorAlert;
use App\Repositories\DeviceSensorAlertRepository;
use InvalidArgumentException;

class AlertHandlerService
{

    public function __construct(
        protected DeviceSensorAlertRepository $alertRepository
    )
    {

    }

    public function handle(array $payload, int $timestamp): void
    {
        $sensorUuids = array_unique(array_keys($payload));
        $alerts = $this->alertRepository->findNotTriggeredAlertsBySensors($sensorUuids, ['sensor.device'], [DeviceSensorAlert::DOWNTIME_TYPE]);

        /** @var DeviceSensorAlert $alert */
        foreach ($alerts as $alert) {
            $alertType = $alert->getAttribute('type');
            $sensorUuid = $alert->getAttribute('device_sensor_uuid');
            $sensorValue = $payload[$sensorUuid];

            $alertHandler = $this->getHandler($alertType);
            $alertHandler->handle($alert, $sensorValue, $timestamp);
        }
    }

    public function getHandler(string $type): AbstractAlertHandler
    {
        return match ($type) {
            DeviceSensorAlert::RANGE_TYPE => app(RangeAlertHandler::class),
            default => throw new InvalidArgumentException("Invalid alert type: $type, provided to getHandler"),
        };
    }

}