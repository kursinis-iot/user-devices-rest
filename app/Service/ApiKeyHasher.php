<?php

namespace App\Service;

class ApiKeyHasher
{
    public function hash(string $apiKey): string
    {
        return password_hash($apiKey, PASSWORD_ARGON2ID);
    }

    public function verify(string $hash, string $apiKey): bool
    {
        return password_verify($apiKey, $hash);
    }
}
