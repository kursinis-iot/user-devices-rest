<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeviceSensorAlert extends Model
{
    public const string RANGE_TYPE = 'range';
    public const string DOWNTIME_TYPE = 'downtime';
    public const array TYPES = [
        self::RANGE_TYPE,
        self::DOWNTIME_TYPE
    ];

    use HasFactory;
    use HasUuids;

    protected $table = 'device_sensor_alert';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = "string";

    public function sensor(): BelongsTo
    {
        return $this->belongsTo(DeviceSensor::class, 'device_sensor_uuid', 'uuid');
    }

    public function emailRecipients(): HasMany
    {
        return $this->hasMany(DeviceSensorAlertEmailRecipient::class, 'device_sensor_alert_uuid', 'uuid');
    }

    protected $fillable = [
        'uuid',
        'device_sensor_uuid',
        'title',
        'interval',
        'type',
        'payload',
        'last_triggered_at',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'payload' => 'array'
    ];
}