<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Device extends Model
{
    /**
     * These are devices statuses. (All possible status attribute values.)
     */
    const string DEVICE_STATUS_INITIALIZED = "allocated"; //Ready to accept connections.
    const string DEVICE_STATUS_ONLINE = "online"; //Online
    const string DEVICE_STATUS_OFFLINE = "offline"; //Offline

    const array VALID_ORDER_BY = [
        'uuid',
        'title',
        'last_sensor_updated_at',
        'status',
        'created_at',
        'updated_at',
    ];

    use HasUuids;
    use HasFactory;

    protected $table = "device";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    protected function addedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'added_by_user', 'uuid');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class, 'device_user',
            'device_id', 'user_id'
        )->withTimestamps()->withPivot('role')->using(UserDevice::class);
    }

    public function sensors(): HasMany
    {
        return $this->hasMany(DeviceSensor::class, 'device_uuid', 'uuid');
    }

    public function anomalyModel(): BelongsTo
    {
        return $this->belongsTo(AnomalyModel::class, 'anomaly_model_uuid', 'uuid');
    }

    protected $fillable = [
        ...self::VALID_ORDER_BY,
        'toggle_offline_after_seconds',
        'added_by_user',
        'config',
        'api_key_hash',
        'anomaly_model_uuid'
    ];

    protected $hidden = [
        'api_key_hash'
    ];

    protected $casts = [
        'config' => 'array'
    ];
}