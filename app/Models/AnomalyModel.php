<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AnomalyModel extends Model
{
    use HasUuids;

    protected $table = "anomaly_models";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    protected $fillable = [
        'uuid',
        'name',
        'created_at',
        'updated_at'
    ];

    public function devices(): HasMany
    {
        return $this->hasMany(Device::class, 'anomaly_model_uuid', 'uuid');
    }
}