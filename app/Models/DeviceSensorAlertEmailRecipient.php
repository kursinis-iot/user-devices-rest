<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DeviceSensorAlertEmailRecipient extends Model
{
    use HasUuids;

    protected $table = 'device_sensor_alert_email_recipient';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = "string";

    public function alert(): BelongsTo
    {
        return $this->belongsTo(DeviceSensorAlert::class, 'device_sensor_alert_uuid', 'uuid');
    }

    protected $fillable = [
        'device_sensor_alert_uuid',
        'email',
        'first_name',
        'last_name',
        'language',
        'created_at',
        'updated_at',
    ];
}