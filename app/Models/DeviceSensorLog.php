<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DeviceSensorLog extends Model
{
    use HasUuids;

    protected $table = 'device_sensor_log';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = "string";
    public $timestamps = false;

    public function sensor(): BelongsTo
    {
        return $this->belongsTo(DeviceSensor::class, 'device_sensor_uuid', 'uuid');
    }

    protected $fillable = [
        'device_sensor_uuid',
        'data',
        'anomaly',
        'created_at',
    ];

    protected $casts = [
        'data' => 'array',
        'anomaly' => 'boolean',
    ];
}