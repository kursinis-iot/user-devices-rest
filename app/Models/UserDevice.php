<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserDevice extends Pivot
{
    const string ROLE_OWNER = "owner";

    protected $table = 'device_user';

    protected $fillable = [
        'device_id',
        'user_id',
        'role',
        'created_at',
        'updated_at'
    ];
}