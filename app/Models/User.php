<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements Authenticatable
{
    use HasUuids;
    use HasFactory;

    protected $table = "user";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    public function addedDevices(): HasMany
    {
        return $this->hasMany(Device::class, 'added_by_user', 'uuid');
    }

    public function devices(): BelongsToMany
    {
        return $this->belongsToMany(
            Device::class, 'device_user',
            'user_id', 'device_id'
        )->withTimestamps()->withPivot('role');
    }

    protected $fillable = [
        'uuid',
        'first_name', 'last_name',
        'language',
        'created_at', 'updated_at'
    ];

    public function getAuthIdentifierName()
    {
        return 'uuid';
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return null;
    }

    public function getRememberToken()
    {
        return null;
    }

    public function setRememberToken($value)
    {
    }

    public function getRememberTokenName()
    {
        return null;
    }
}