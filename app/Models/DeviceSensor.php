<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeviceSensor extends Model
{
    use HasFactory;
    use HasUuids;

    protected $table = "device_sensor";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    public function alerts(): HasMany
    {
        return $this->hasMany(DeviceSensorAlert::class, 'device_sensor_uuid', 'uuid');
    }

    public function log(): HasMany
    {
        return $this->hasMany(DeviceSensorLog::class, 'device_sensor_uuid', 'uuid');
    }

    public function dayOfLogs(): HasMany
    {
        return $this->log()->where('created_at', ">", Carbon::now()->subDay()->toDateTimeString())->orderBy('created_at', 'ASC');
    }

    public function minuteOfLogs(): HasMany
    {
        return $this->log()->where('created_at', ">", Carbon::now()->subMinute()->toDateTimeString())->orderBy('created_at', 'ASC');
    }

    protected $fillable = [
        'title',
        'device_uuid',
        'jsonpath_query',
        'created_at',
        'updated_at',
    ];
}