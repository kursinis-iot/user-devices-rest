<?php

namespace App\Jobs\Alert;

use App\Jobs\Job;
use App\Models\Device;
use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use App\Repositories\DeviceSensorAlertRepository;
use App\Service\Alert\AlertTriggererService;
use Carbon\Carbon;

class TriggerDowntimeAlertsJob extends Job
{
    public function handle(): void
    {
        /** @var DeviceSensorAlertRepository $alertRepository */
        $alertRepository = app(DeviceSensorAlertRepository::class);
        $toTrigger = $alertRepository->findDowntimeAlertsToTrigger(['sensor.device']);
        if ($toTrigger->isEmpty())
            return;

        /** @var AlertTriggererService $alertTriggerer */
        $alertTriggerer = app(AlertTriggererService::class);

        /** @var DeviceSensorAlert $alert */
        foreach ($toTrigger as $alert) {
            /** @var DeviceSensor $sensor */
            $sensor = $alert->getAttribute('sensor');
            /** @var Device $device */
            $device = $sensor->getAttribute('device');

            $lastUpdatedAt = $device->getAttribute('last_sensor_updated_at');
            $interval = $alert->getAttribute('payload');

            $alertTriggerer->failAlert(
                $alert,
                null,
                Carbon::now('UTC')->getPreciseTimestamp(3),
                //TODO: add multilingualism to message.
                "Sensor data was last received at $lastUpdatedAt, which is more than $interval seconds before now."
            );
        }
    }
}