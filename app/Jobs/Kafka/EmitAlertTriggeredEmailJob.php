<?php

namespace App\Jobs\Kafka;

use App\Jobs\Job;
use App\Models\Device;
use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use App\Models\DeviceSensorAlertEmailRecipient;
use Kudze\KafkaConsumerProducer\Services\KafkaProducer;
use RdKafka\Exception;

class EmitAlertTriggeredEmailJob extends Job
{
    public function __construct(
        protected DeviceSensorAlert $alert,
        protected ?array            $sensorValue = null,
        protected ?int              $timestamp = null,
        protected ?string           $message = null,
    )
    {

    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $this->alert->loadMissing(['sensor.device']);

        /** @var DeviceSensor $sensor */
        $sensor = $this->alert->getAttribute('sensor');
        /** @var Device $device */
        $device = $sensor->getAttribute('device');
        $recipients = $this->alert->emailRecipients()->get();

        /** @var KafkaProducer $producer */
        $producer = app(KafkaProducer::class);

        /** @var DeviceSensorAlertEmailRecipient $email */
        foreach ($recipients as $email)
            $producer->produce(
                json_encode([
                    'from' => env('MAILER_FROM'),
                    'to' => $email->getAttribute('email'),
                    'template' => 'ALERT_TRIGGER',
                    'payload' => [
                        'sensor' => [
                            'uuid' => $sensor->getKey(),
                            'title' => $sensor->getAttribute('title'),
                            'device' => [
                                'uuid' => $device->getKey(),
                                'title' => $device->getAttribute('title')
                            ]
                        ],
                        'alert' => [
                            'uuid' => $this->alert->getKey(),
                            'title' => $this->alert->getAttribute('title'),
                            'interval' => $this->alert->getAttribute('interval')
                        ],
                        'sensorValue' => $this->sensorValue,
                        'timestamp' => $this->timestamp,
                        'message' => $this->message,
                        'recipient' => [
                            'firstName' => $email->getAttribute('first_name'),
                            'lastName' => $email->getAttribute('last_name')
                        ]
                    ],
                    'language' => $email->getAttribute('language')
                ]),
                env('KAFKA_MAIL_TOPIC')
            );

        $producer->flush(1000);
    }
}