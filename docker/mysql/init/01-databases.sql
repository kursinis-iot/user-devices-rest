CREATE DATABASE IF NOT EXISTS `user_devices_rest_test`;

CREATE USER `user_devices_rest_test_user`@'%' IDENTIFIED BY 'changeme';
GRANT ALL PRIVILEGES ON `user_devices_rest_test`.* TO 'user_devices_rest_test_user'@'%';
