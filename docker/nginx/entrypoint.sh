#!/bin/sh

# We replace $PHP_UPSTREAM in nginx config.
sed -i "s/\$PHP_UPSTREAM/$PHP_UPSTREAM/g" /etc/nginx/conf.d/default.conf

exec nginx -g "daemon off;"