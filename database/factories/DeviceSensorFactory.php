<?php

namespace Database\Factories;

use App\Models\Device;
use App\Models\DeviceSensor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceSensorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeviceSensor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->words(3, true),
            'device_uuid' => fn() => Device::query()->first()?->getKey() ?? null,
            'jsonpath_query' => '$.sensors.temperature',
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
        ];
    }
}
