<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $gender = $this->faker->boolean();

        return [
            'uuid' => $this->faker->uuid,
            'first_name' => $this->faker->firstName($gender),
            'last_name' => $this->faker->lastName($gender),
            'language' => $this->faker->randomElement(['lt', 'en']),
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
        ];
    }
}
