<?php

namespace Database\Factories;

use RuntimeException;
use App\Models\DeviceSensor;
use App\Models\DeviceSensorAlert;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeviceSensorAlertFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DeviceSensorAlert::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $type = $this->faker->randomElement(DeviceSensorAlert::TYPES);

        return [
            'device_sensor_uuid' => fn() => DeviceSensor::query()->first()?->getKey() ?? null,
            'title' => $this->faker->words(3, true),
            'interval' => $this->faker->numberBetween(1800, 7200),
            'type' => $type,
            'payload' => $this->payloadDefinition($type),
            'last_triggered_at' => $this->faker->boolean() ? Carbon::parse($this->faker->dateTime) : null,
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
        ];
    }

    protected function payloadDefinition(string $type): string
    {
        $payload = match ($type) {
            DeviceSensorAlert::DOWNTIME_TYPE => $this->faker->numberBetween(1800, 7200),
            DeviceSensorAlert::RANGE_TYPE => [
                'min' => $this->faker->numberBetween(0, 10),
                'max' => $this->faker->numberBetween(90, 100),
            ],
            default => throw new RuntimeException("$type is unknown alert type!"),
        };

        return json_encode($payload);
    }
}
