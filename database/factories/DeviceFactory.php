<?php

namespace Database\Factories;

use App\Models\Device;
use App\Service\ApiKeyHasher;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DeviceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Device::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        /** @var ApiKeyHasher $hasher */
        $hasher = app(ApiKeyHasher::class);

        return [
            'title' => $this->faker->words(3, true),
            'last_sensor_updated_at' => $this->faker->boolean() ? Carbon::parse($this->faker->dateTime) : null,
            'status' => Device::DEVICE_STATUS_INITIALIZED,
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
            'toggle_offline_after_seconds' => $this->faker->numberBetween(60, 3600),
            'api_key_hash' => $hasher->hash(Str::orderedUuid()),
            'added_by_user' => null,
            'config' => '{}'
        ];
    }
}
