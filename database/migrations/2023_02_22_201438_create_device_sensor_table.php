<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_sensor', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->timestamps();

            $table->foreignUuid('device_uuid')->constrained('device', 'uuid')->cascadeOnDelete();
            $table->string('title');
            $table->string('jsonpath_query');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_sensor');
    }
};
