<?php

use App\Models\Device;
use App\Service\ApiKeyHasher;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //Workaround for already created devices.
        Schema::table('device', function (Blueprint $table) {
            $table->string('api_key_hash')->nullable();
        });

        /** @var ApiKeyHasher $hasher */
        $hasher = app(ApiKeyHasher::class);
        Device::query()->chunk(100, function (Collection $collection) use ($hasher) {
            /** @var Device $device */
            foreach ($collection as $device) {
                $device->setAttribute('api_key_hash', $hasher->hash($device->getKey()));
                $device->save();
            }
        });

        Schema::table('device', function (Blueprint $table) {
            $table->string('api_key_hash')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('device', function (Blueprint $table) {
            $table->dropColumn('api_key_hash');
        });
    }
};
