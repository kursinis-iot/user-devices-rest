<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->timestamps();

            $table->integer("kafka_sensor_partition")->unique()->nullable();
            $table->string('title', 255);
            $table->integer('toggle_offline_after_seconds');
            $table->foreignUuid('added_by_user')->nullable()->constrained("user", "uuid")->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device');
    }
};
