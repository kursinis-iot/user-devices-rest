<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('anomaly_models', function (Blueprint $table) {
            $table->dropColumn('uuid');
        });

        Schema::table('anomaly_models', function (Blueprint $table) {
            $table->uuid()->primary();
        });

        Schema::table('device', function (Blueprint $table) {
            $table->foreignUuid('anomaly_model_uuid')->nullable()->constrained('anomaly_models', 'uuid')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('device', function (Blueprint $table) {
            $table->dropForeign(['anomaly_model_uuid']);
            $table->dropColumn('anomaly_model_uuid');
        });

        Schema::table('anomaly_models', function (Blueprint $table) {
            $table->dropPrimary(['uuid']);
            $table->dropColumn('uuid');
        });

        Schema::table('anomaly_models', function (Blueprint $table) {
            $table->uuid();
        });
    }
};
