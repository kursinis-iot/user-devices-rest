<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_user', function (Blueprint $table) {
            $table->timestamps();

            $table->foreignUuid("device_id")->constrained("device", 'uuid')->cascadeOnDelete();
            $table->foreignUuid("user_id")->constrained("user", 'uuid')->cascadeOnDelete();
            $table->string("role");

            $table->primary(['device_id', 'user_id'], 'device_user_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_user');
    }
};
