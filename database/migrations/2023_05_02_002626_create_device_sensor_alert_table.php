<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_sensor_alert', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignUuid('device_sensor_uuid')->constrained('device_sensor', 'uuid')->cascadeOnDelete();
            $table->string('title');
            $table->integer("interval");

            /**
             * Used to define type for the alert.
             */
            $table->string('type');

            /**
             * Used to define additional infos for alert.
             * For example if type is range payload could be min max bounds.
             */
            $table->json('payload');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_sensor_alert');
    }
};