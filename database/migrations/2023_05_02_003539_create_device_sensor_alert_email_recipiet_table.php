<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_sensor_alert_email_recipient', function (Blueprint $table) {
            $table->uuid('device_sensor_alert_uuid');
            $table->foreign('device_sensor_alert_uuid', 'device_sensor_alert_uuid_email_fk')->on('device_sensor_alert')->references('uuid')->cascadeOnDelete();
            $table->string('email', 255);
            $table->timestamps();

            $table->primary(['device_sensor_alert_uuid', 'email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_sensor_alert_email_recipient');
    }
};