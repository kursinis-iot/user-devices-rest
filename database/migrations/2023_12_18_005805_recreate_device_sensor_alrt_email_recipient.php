<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('device_sensor_alert_email_recipient');
        Schema::dropIfExists('device_sensor_log');

        Schema::create('device_sensor_alert_email_recipient', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->uuid('device_sensor_alert_uuid');
            $table->foreign('device_sensor_alert_uuid', 'device_sensor_alert_uuid_email_fk')->on('device_sensor_alert')->references('uuid')->cascadeOnDelete();
            $table->string('email', 255);
            $table->timestamps();

            $table->unique(['device_sensor_alert_uuid', 'email'], 'uq_alert_uuid_email');
        });

        Schema::create('device_sensor_log', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->foreignUuid('device_sensor_uuid')->constrained('device_sensor', 'uuid')->cascadeOnDelete();
            $table->timestamp('created_at');
            $table->json('data');

            $table->unique(['device_sensor_uuid', 'created_at'],  'uq_sensor_uuid_created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        throw new Exception("Not supported!");
    }
};
