<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_sensor_log', function (Blueprint $table) {
            $table->foreignUuid('device_sensor_uuid')->constrained('device_sensor', 'uuid')->cascadeOnDelete();
            $table->timestamp('created_at');
            $table->json('data');

            $table->primary(['device_sensor_uuid', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_sensor_log');
    }
};
